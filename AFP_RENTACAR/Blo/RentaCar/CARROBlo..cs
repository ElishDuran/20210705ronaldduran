﻿using Model.RentaCar;
using System;
using Dao.RentaCar;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Blo.RentaCar
{
   public  class CARRO:ICARRO
    {
        private ICARRODao _carroDao;

        public CARRO(ICARRODao carroDao)
        {
            _carroDao = carroDao;
        }
        // <summary>
        /// Metodo que obtenie todos los carros
        /// </summary>
        /// <returns>lista de carros</returns>
        public List<OutCarroModel> GetAllCarro()
        {
            return _carroDao.GetAllCarro();
        }
    }
}
