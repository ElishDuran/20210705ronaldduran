﻿using Ninject;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Blo.RentaCar;
using Model;
using Model.RentaCar;
using System.Web.Http;

namespace BackEnd.Controllers.RentaCar
{
    public class CarroController : ApiController
    {
     
        [Inject]
        public ICARRO _carroBlo { get; set; }

        /// <summary>
        /// Metodo para obtener todos los examen
        /// </summary>
        /// <returns></returns>
        [System.Web.Http.HttpGet]
        public Mensajes GetAllCarros()
        {
            List<OutCarroModel> lista = new List<OutCarroModel>();
            Mensajes msjDto = new Mensajes();

            try
            {
                lista = _carroBlo.GetAllCarro();
                msjDto.Respuesta = lista;
                msjDto.Mensaje = "";
                msjDto.Codigo = 0;
            }
            catch (Exception ex)
            {
                msjDto.Mensaje = "Problemas al obtener los examenes";
                msjDto.Codigo = 1;
            }
            return msjDto;
        }
    }
}