﻿using Model.RentaCar;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Oracle.ManagedDataAccess.Client;
using System.Threading.Tasks;
using Dapper;
using System.Data;

namespace Dao.RentaCar
{
    public class CARRODao: Conexion,ICARRODao
    {
        // <summary>
        /// Metodo que obtenie todos los carros
        /// </summary>
        /// <returns>lista de carros</returns>
        public List<OutCarroModel> GetAllCarro()
        {
            List<OutCarroModel> listaUsuarios = new List<OutCarroModel>();


            using (var db = new OracleConnection(GetConnectionString()))
            {
                try
                {
                    var query = "select * from SYS.CARRO ";

                    listaUsuarios = db.Query<OutCarroModel>(query).ToList();

                    //OracleCommand cmd = new OracleCommand(query, db);
                    //db.Open();
                    //OracleDataReader reader = cmd.ExecuteReader();
                    //try
                    //{
                    //    while (reader.Read())
                    //    {
                    //        var a = reader.GetString(1);
                    //    }
                    //}
                    //finally
                    //{
                    //    // always call Close when done reading.
                    //    reader.Close();
                    //}

                }
                catch (Exception ex)
                {
                    var a = ex;
                }
            }
            return listaUsuarios;
        }
    }
}
