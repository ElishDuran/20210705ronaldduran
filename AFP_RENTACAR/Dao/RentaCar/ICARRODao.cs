﻿using Model.RentaCar;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dao.RentaCar
{
    public interface ICARRODao
    {
        // <summary>
        /// Metodo que obtenie todos los carros
        /// </summary>
        /// <returns>lista de carros</returns>
        List<OutCarroModel> GetAllCarro();
    }
}
