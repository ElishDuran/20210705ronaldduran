﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;

using System.Threading.Tasks;

namespace Dao
{
   public class Conexion
    {
        /// <summary>
        /// Metodo para obtener la conexión a la base de datos
        /// </summary>
        /// <returns>Cadena de conexión</returns>
        public string GetConnectionString()
        {
            string conexion = "";
            try
            {
                conexion = ConfigurationManager.AppSettings["servidorBDOracle"];
            }
            catch (Exception ex)
            {
                throw new Exception("Error al obtener cadena de conexión");
            }
            return conexion;
        }
    }
}
