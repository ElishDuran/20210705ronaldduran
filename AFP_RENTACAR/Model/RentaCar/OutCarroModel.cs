﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model.RentaCar
{
    public class OutCarroModel
    {
        public int IDCARRO { get; set; }
        public string MARCA { get; set; }
        public string MODELO { get; set; }
        public string PLACA { get; set; }
        public string TIPO_SERVICIO { get; set; }
        public double PRECIO_VENTA { get; set; }
        public double PRECIO_ALQUILER { get; set; }
    }
}
