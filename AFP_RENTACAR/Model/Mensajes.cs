﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model
{
    /// <summary>
    /// Clase que permitira los envios de los resultados al cliente, en donde la respuesta contendra el objeto
    /// que se pretende enviar al usuario, Codigo: indicara el estado de la transacción si es 0 es exitoso
    /// caso contrario es porque ocurrio un error y el Mensaje  sera nulo  si es exitoso sino contendra el mensaje
    /// detallando el error.
    /// </summary>
    public class Mensajes
    {
            public object Respuesta { get; set; }
            public int Codigo { get; set; }
            public string Mensaje { get; set; }
        
    }
}
